var game = new Phaser.Game(1600, 900, Phaser.AUTO, '');
game.state.add('gameplay', gameplayState);
game.state.add("gameOver", gameOverState);
game.state.add("menu", menuState);
game.state.start('menu');