let MAP_WIDTH = 32;
let ENEMIES_COUNT = 4;

var player;
var enemies;
var enemiesSpritesGroup;
var controls;
var backgroundlayer;

var bullet;
var bullets;
var bulletTime = 0;
let FIREARM_LOAD = 20;

var tvPlayerHpIndicator;
var imageHeart;

var killedEnemies = 0;
var currentLevel = 1;

var gameplayState = {
    preload : function() {
        game.load.tilemap('tilemap', 'assets/map.csv', null, Phaser.Tilemap.CSV);
        game.load.image('tileset', 'assets/tileset.png');
        game.load.spritesheet('player', 'assets/player.png', 32, 48);
        game.load.spritesheet('enemy', 'assets/enemy.png', 64, 64);
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('heart', 'assets/heart.png');
    },

	create : function() {
        this.initMap();
        this.initControls();
        this.initPlayer();
        this.initEnemies();
        this.initBullets();
        this.initPhysics();
        this.initUi();
    },

    update : function() {

        game.physics.arcade.overlap(bullets, enemiesSpritesGroup, this.handleCollisionBetweenBulletAndEnemy, null, this);
        game.physics.arcade.collide(bullets, backgroundlayer, this.handleCollisionBetweenBulletAndWall);
        game.physics.arcade.collide(player.sprite, backgroundlayer);
        game.physics.arcade.collide(enemiesSpritesGroup, enemiesSpritesGroup);
        game.physics.arcade.collide(enemiesSpritesGroup, backgroundlayer);
        game.physics.arcade.collide(enemiesSpritesGroup, player.sprite, this.handleCollisionBetweenPlayerAndEnemy);

        player.reset();

        if (controls.down.isDown){
            player.setDirectionY(DOWN);
        }

        if (controls.up.isDown){
            player.setDirectionY(UP);
        }
        
        if(controls.left.isDown){
            player.setDirectionX(LEFT);
        }

        if(controls.right.isDown){
            player.setDirectionX(RIGHT);
        }

        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
        {
            this.shoot();
        }

        player.move();
        player.playAnimation();

        this.updateEnemies();
        this.updatePlayerHpIndicator();
        this.updateKilledEnemiesCounter();
		
		if (enemiesSpritesGroup.countLiving() <= 0) {
			this.increaseLevelIfNeeded();
        }
    },

    initMap : function() {
        map = game.add.tilemap('tilemap', 32, 32);
        map.addTilesetImage('tileset');

        backgroundlayer = map.createLayer(0);
        map.setCollision(3, true, backgroundlayer);
        map.setCollision(4, true, backgroundlayer);
        map.setCollision(5, true, backgroundlayer);
        map.setCollision(6, true, backgroundlayer);
        map.setCollision(7, true, backgroundlayer);
        map.setCollision(17, true, backgroundlayer);
        map.setCollision(18, true, backgroundlayer);
        map.setCollision(19, true, backgroundlayer);
        map.setCollision(20, true, backgroundlayer);
        map.setCollision(21, true, backgroundlayer);
        map.setCollision(22, true, backgroundlayer);
        map.setCollision(23, true, backgroundlayer);
        map.setCollision(24, true, backgroundlayer);
        map.setCollision(25, true, backgroundlayer);
        map.setCollision(26, true, backgroundlayer);
        map.setCollision(27, true, backgroundlayer);
        map.setCollision(28, true, backgroundlayer);
        backgroundlayer.resizeWorld(true);
    },

    initPlayer : function() {
        player = new Player(game.add.sprite(game.world.centerX - 100, game.world.centerY - 50, 'player'));
    },

    initEnemies: function() {
        enemies = this.createRandomEnemies(ENEMIES_COUNT);
        enemiesSpritesGroup = game.add.group();

        for (var i = enemies.length - 1; i >= 0; i--) {
            enemiesSpritesGroup.add(enemies[i].sprite);
        }
    },

    initBullets : function() {
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;

        for (var i = 0; i < FIREARM_LOAD; i++) {
            var b = bullets.create(0, 0, 'bullet');
            b.name = 'bullet' + i;
            b.exists = false;
            b.visible = false;
            b.checkWorldBounds = true;
            b.events.onOutOfBounds.add(this.removeBullet, this);
        }
    },

    initControls : function() {
        controls = game.input.keyboard.createCursorKeys();
        game.input.keyboard.addKeyCapture([ Phaser.Keyboard.SPACEBAR ]);
    },

    initPhysics : function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.enable([backgroundlayer, player.sprite, enemiesSpritesGroup], Phaser.Physics.ARCADE);
    },

	increaseLevelIfNeeded : function() {
		if (enemiesSpritesGroup.countLiving() <= 0) {
			currentLevel++;
			this.spawnNewEnemies(currentLevel * ENEMIES_COUNT);
        }
	},
	
	spawnNewEnemies : function(enemiesCount) {
		enemies = this.createRandomEnemies(enemiesCount);
		
        for (var i = enemies.length - 1; i >= 0; i--) {
            enemiesSpritesGroup.add(enemies[i].sprite);
        }
		
		game.physics.enable([enemiesSpritesGroup], Phaser.Physics.ARCADE);
	},
	
    createRandomEnemies(enemiesCount) {
        var randomlyPlacedEnemies = [];

        for (var i = 0; i < enemiesCount; ++i) {
            var enemy = new Enemy(game.add.sprite(Math.random() * 100 * 16, Math.random() * 100 % MAP_WIDTH, 'enemy'));
            randomlyPlacedEnemies.push(enemy);
        }

        return randomlyPlacedEnemies;
    },

    updateEnemies : function() {
        for (var i = 0; i < enemies.length; i++) {
            enemies[i].moveToTarget(player.x(), player.y());
        }
    },

    shoot : function() {

        if (game.time.now > bulletTime)
        {
            bullet = bullets.getFirstExists(false);
            if (bullet)
            {

                if (player.getFireDirectionX() != NONE) {
                    bullet.reset(player.sprite.x + 50 * player.getFireDirectionX(),
                                player.sprite.y - 8 * player.getFireDirectionX());

                    bullet.angle = player.getFireDirectionX() * 90;
                    bullet.body.velocity.y = 0;
                    bullet.body.velocity.x = player.getFireDirectionX() * 300;
                }

                if (player.getFireDirectionY() != NONE) {
                    bullet.angle = 0;
                    bullet.reset(player.sprite.x - 8 + 0 * player.getFireDirectionY(),
                                player.sprite.y - 22 + 32 * player.getFireDirectionY());

                    bullet.body.velocity.x = 0;
                    bullet.body.velocity.y = player.getFireDirectionY() * 300;
                }

                bulletTime = game.time.now + 150;
            }
        }
    },

    removeBullet : function(bullet) {
        bullet.kill();
    },

    handleCollisionBetweenPlayerAndEnemy : function(obj1, obj2) {
        if (math.round(game.time.totalElapsedSeconds()) % 2 == 0) {
            player.gotHit();
        }

        if (player.hp <= 0) {
            game.state.start('gameOver');
        }
    },

    handleCollisionBetweenBulletAndWall : function(bullet, wall) {
        bullet.kill();
    },

    handleCollisionBetweenBulletAndEnemy : function(bullet, enemy) {
        bullet.kill();
        enemy.kill();
        killedEnemies = killedEnemies + 1;
    },

    updatePlayerHpIndicator : function() {
        var playerHpPercantage = player.hp / player.maxHp * 100;

        tvPlayerHpIndicator.setText(Math.round(playerHpPercantage));

        if (playerHpPercantage < 30) {
            tvPlayerHpIndicator.fill = '#ff0000';
        } else if (playerHpPercantage < 60) {
            tvPlayerHpIndicator.fill = '#ffff00';
        } else {
            tvPlayerHpIndicator.fill = '#43d637';
        }
    },

    initUi : function() {
        this.initPlayerHpIndicator();
        this.initKilledEnemiesIndicator();
    },

    initPlayerHpIndicator : function() {
        imageHeart = game.add.sprite(game.camera.width - 100, game.camera.height -100, "heart");
        imageHeart.fixedToCamera = true;
        imageHeart.cameraOffset.setTo(game.camera.width - 100, game.camera.height -95);
        imageHeart.scale.setTo(0.25, 0.25);

        tvPlayerHpIndicator = game.add.text(5, 5, "100");
        tvPlayerHpIndicator.fixedToCamera = true;
        tvPlayerHpIndicator.anchor.set(0.5);
        tvPlayerHpIndicator.cameraOffset.setTo(game.camera.width - 165, game.camera.height -60);
        tvPlayerHpIndicator.align = 'center';
        tvPlayerHpIndicator.font = 'Arial Black';
        tvPlayerHpIndicator.fontSize = 55;
        tvPlayerHpIndicator.fontWeight = 'bold';
        tvPlayerHpIndicator.stroke = '#000000';
        tvPlayerHpIndicator.strokeThickness = 6;
        tvPlayerHpIndicator.fill = '#43d637';
    },

    initKilledEnemiesIndicator() {
        text = game.add.text(5, 5, "ENEMIES KILLED: " + killedEnemies);
        text.fixedToCamera  = true;
        text.anchor.set(0.0);
        text.align = 'center';
        text.font = 'Arial Black';
        text.fontSize = 20;
        text.fontWeight = 'bold';
        text.stroke = '#000000';
        text.strokeThickness = 6;
        text.fill = '#43d637';
    },

    updateKilledEnemiesCounter() {
        text.setText("ENEMIES KILLED: " + killedEnemies);
    }
}
